package guy.droid.com.wearnotification;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.ActionPage;
import android.view.View;
import android.widget.Toast;

/**
 * Created by admin on 9/21/2016.
 */

public class WearAction extends WearableActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionPage actionPage = (ActionPage)findViewById(R.id.wearaction);
        actionPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"TOUCHED",Toast.LENGTH_LONG).show();
            }
        });


    }

}
