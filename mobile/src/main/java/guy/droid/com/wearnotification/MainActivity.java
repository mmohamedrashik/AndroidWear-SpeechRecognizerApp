package guy.droid.com.wearnotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.wearable.WearableListenerService;

public class MainActivity extends AppCompatActivity{
Button notify;
    NotificationCompat.Builder mBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notify = (Button)findViewById(R.id.notify);
        mBuilder = new NotificationCompat.Builder(this);

        notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startService(new Intent(getApplicationContext(),WearListCallListenerService.class));

                mBuilder.setSmallIcon(android.R.drawable.arrow_up_float);
                mBuilder.setContentTitle("Notification Alert, Click Me!");
                mBuilder.setContentText("Hi, This is Android Notification Detail!");
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender();

                mBuilder.extend(wearableExtender);
                mNotificationManager.notify(100, mBuilder.build());
            }
        });
    }

}
